import React, { Component } from "react";

export class Error extends Component {
	render() {
		return <div>{this.props.message}</div>;
	}
}

export default Error;
