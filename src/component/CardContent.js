import React, { Component } from "react";

export class CardContent extends Component {
	render() {
		const { name, main, weather } = this.props.weatherData;
		return (
			<div id="weatherCard">
				<h2 id="weatherName">Name: {name}</h2>
				<h2 id="weatherTemp">Temp: {main.temp}&deg;C</h2>
				<h2 id="weatherDesc">Desc: {weather[0].description}</h2>
				<img
					id="weatherIcon"
					src={`https://openweathermap.org/img/wn/${weather[0].icon}@2x.png`}
					alt="weather-icon"
				/>
			</div>
		);
	}
}

export default CardContent;
