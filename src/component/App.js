import React, { Component } from "react";
import CardContent from "./CardContent";
import Error from "./Error";
import Input from "./Input";

const status = {
	LOADING: 0,
	SUCCESS: 1,
	FAILED: -1,
};

class App extends Component {
	state = {
		weatherData: {},
		error: "",
		status: status.LOADING,
	};

	getWeatherByCity = (city) => {
		fetch(
			`https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${process.env.REACT_APP_APP_ID}`
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.cod === 200) {
					// resetting error
					this.setState({
						weatherData: data,
						error: "",
						status: status.SUCCESS,
					});
				} else {
					this.setState({
						error: data.message,
						status: status.FAILED,
					});
				}
			})
			.catch((error) => {
				this.setState({
					error: error.message,
					status: status.FAILED,
				});
			});
	};

	getWeatherByGeo = (lat, lon) => {
		fetch(
			`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&appid=${process.env.REACT_APP_APP_ID}`
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.cod === 200) {
					// resetting error
					this.setState({
						weatherData: data,
						error: "",
						status: status.SUCCESS,
					});
				} else {
					this.setState({
						error: data.message,
						status: status.FAILED,
					});
				}
			})
			.catch((error) => {
				this.setState({
					error: error.message,
					status: status.FAILED,
				});
			});
	};

	success = (position) => {
		const { latitude, longitude } = position.coords;
		this.getWeatherByGeo(latitude, longitude);
	};

	error = (error) => {
		this.setState({
			error: error.message,
			status: status.FAILED,
		});
	};

	componentDidMount() {
		navigator.geolocation.getCurrentPosition(this.success, this.error);
	}

	render() {
		return (
			<div className="container">
				<div id="searchIcon">
					<Input getWeatherByCity={this.getWeatherByCity} id="" />
				</div>

				{this.state.status === status.LOADING ? <h3>Loading...</h3> : null}
				{this.state.status === status.FAILED ? (
					<Error message={this.state.error} />
				) : null}

				{this.state.status === status.SUCCESS ? (
					<CardContent weatherData={this.state.weatherData} />
				) : null}
			</div>
		);
	}
}

export default App;
